<?php

/**
 * User: ossama.salimi@gmail.com
 * Date: 4/7/14
 * Time: 4:07 PM
 */
class PIXPOINTS_API
{
    const DB_TABLE = "pixpoints"; //legacy table name

    const STATUS_WAITING = 0;
    const STATUS_CONFIRMED = 1;
    const STATUS_EXPIRED = 2;
    const STATUS_REJECTED = 3; # *new - the user doesn't want to join your site.

    const DB_VERSION = "1.2"; # db version of DB_TABLE

    /**
     * Instance of this class.
     *
     * @since    1.0.0
     *
     * @var      object
     */
    protected static $instance = null;

    /**
     *
     */
    function __construct()
    {

    }

    /**
     * get_instance function.
     *
     * @access public
     * @static
     * @return $instance
     */
    public static function get_instance()
    {

        // If the single instance hasn't been set, set it now.
        if (null == self::$instance) {
            self::$instance = new self;
            // do the install check
            self::install();

        }

        return self::$instance;
    }


    /**
     * install check if the plugin is installed. if not, install invitation table.
     *
     * @access public
     * @return void
     */
    public static function install()
    {
        global $wpdb;
        $installed_ver = get_site_option("pixpoints_db_version");

        if (version_compare(self::DB_VERSION, $installed_ver)) {

            // order, label, optionname, type, display, required, native, checked value, checked by default

            update_option('pp_like_page_points', 0);
            update_option('pp_like_post_points', 0);
            update_option('pp_comment_points', 0);
            update_option('pp_share_points', 0);
            update_option('pp_follow_points', 0);
            update_option('pp_tweet_points', 0);
            update_option('pp_google_points', 0);
            update_option('pp_email_points', 0);
            update_option('pp_enable_popup', 0);


            $sql = "CREATE TABLE " . $wpdb->base_prefix . self::DB_TABLE . " (
			  id bigint(20) NOT NULL AUTO_INCREMENT,
			  uid bigint(20) NOT NULL,
			  type VARCHAR(256) NOT NULL,
			  data TEXT NOT NULL,
			  points bigint(20) NOT NULL,
			  timestamp bigint(20) NOT NULL,
			  UNIQUE KEY id (id)
			);";


            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
            update_site_option("pixpoints_db_version", self::DB_VERSION);

        }
    }

    /** Adds transaction to logs database */

    public static function log($type, $uid, $points, $data)
    {
        global $wpdb;

        $userinfo = get_userdata($uid);
        if ($userinfo->user_login == '') {
            return false;
        }
        if ($points == 0 && !in_array($type, array('reset', 'new_post'))) {
            return false;
        }
        if ($type == 'new_post') {
            $sql = 'SELECT * FROM `' . $wpdb->base_prefix . self::DB_TABLE . '` WHERE data = ' . $data . ' AND type = "new_post"';
            $tmp = $wpdb->get_results($sql, ARRAY_A);
            if (count($tmp)) return false;
        }

        $timestamp = time();

        $res = $wpdb->query("INSERT INTO `" . $wpdb->base_prefix . self::DB_TABLE . "` (`id`, `uid`, `type`, `data`, `points`, `timestamp`)
				  VALUES (NULL, '" . $uid . "', '" . $type . "', '" . $data . "', '" . $points . "', " . time() . ");");

        $total = (int)get_user_meta($uid, 'pp_total', true);
        update_user_meta($uid, 'pp_total', ($total + $points));

        // PUSHER
        if ($res && get_option('pp_enable_pusher') === 'on') {

            $meta_data = get_user_meta($uid);
            $img = '';
            if (isset($meta_data['facebook_avatar']))
                $img = "<img width='50px' height='50px' src='{$meta_data['facebook_avatar'][0]}' class='avatar-50' />";
            else
                $img = get_avatar($uid, '50');

            $first_name = (isset($meta_data['first_name'][0])) ? $meta_data['first_name'][0] : '';
            $nickname = (isset($meta_data['nickname'][0])) ? $meta_data['nickname'][0] : 'inconnu';

            $site_activity = array('facebook_page_like', 'twitter_follow', 'email_points', 'register_points');


            $url = null;
            $post_id = null;
            $thetitle = '--';
            $post_id = $data;


            $msg_cat = '';
            $cat = null;
            // Check if commentaire
            if (is_type_comment($type)) {

                $post_id = explode('_', $data);
                $post_id = (int)$post_id[0];

            }


            if ($post_id && ctype_digit($post_id)) {
                if (is_type_category($type)) {
                    $permalink = get_category_link($post_id);
                    $thetitle = get_cat_name($post_id);

                } elseif (is_type_post($type)) {
                    $permalink = get_permalink($post_id);

                    $category = get_the_category($post_id);
                    if ($category) {
                        $cat = ($category[0]->category_parent) ? $category[0]->category_parent : $category[0]->cat_ID;
                        $thetitle = get_option('cat_activity_' . $cat);

                    }


                }
                $url = '<a target="_blank" href="' . $permalink . '">' . $thetitle . '</a>';


                $msg = (is_type_category($type) ? get_cat_name($post_id) : get_the_title($post_id));
            }


            $msg = stripslashes(get_option($type)) . ' ' . $url;


            $str = '<li class="activity-feed-item" style="display: none;" >';
            $str .= '<div class="avatar-activity">' . $img . '</div>';
            $str .= '<div class="info-activity">
                    <span class="time-activity legend">' . "à l'instant" . '<p class="user-name">' . ((trim($first_name) != '') ? $first_name : $nickname) . '</p>';
            $str .= '<p>';
            if ($points) {
                $str .= '<span class="pointz">+ ' . $points . 'points</span>';
            }


            $str .= $msg . '</p>';
            $str .= '</span></div></li>';

            $pusher = new Pusher(APP_KEY, APP_SECRET, APP_ID);
            $pusher->trigger('pixactivity_channel', 'new_activity', $str);

            do_action('pix_log', $type, $uid, $points, $data);


        }
        return true;
    }

    public static function getPointsDb($uid, $type = null, $data = null)
    {
        global $wpdb;

        $sql = 'SELECT SUM(points) FROM `' . $wpdb->base_prefix . self::DB_TABLE . '` where  `uid`=' . $uid . ' ' . (($type) ? " AND type = '" . $type . "'" : "") . (($data) ? ' AND data = ' . $data : '');
        return (int)$wpdb->get_var($sql);
    }

    public static function getPoints24Hours($uid)
    {
        global $wpdb;

        $sql = "SELECT SUM(`points`) FROM `" . $wpdb->base_prefix . self::DB_TABLE . "` WHERE `uid` = $uid AND type != 'register_points' AND `timestamp` >= UNIX_TIMESTAMP(NOW()- INTERVAL 1 DAY)";
        return (int)$wpdb->get_var($sql);
    }


    public static function getAllActivityDb($nbr = null)
    {
        global $wpdb;

        $limit = ($nbr) ? 'limit ' . $nbr : '';

        $sql = 'SELECT * FROM `' . $wpdb->base_prefix . self::DB_TABLE . '` WHERE type != "post_view" ORDER BY timestamp DESC ' . $limit;
        return $wpdb->get_results($sql, ARRAY_A);
    }


    public static function removePoints($type, $uid, $data)
    {
        global $wpdb;

        $res = $wpdb->query("DELETE FROM `" . $wpdb->base_prefix . self::DB_TABLE . "` WHERE uid = " . $uid . " AND type = '" . $type . "' AND data = '" . $data . "'");
        update_user_meta($uid, 'pp_total', self::get_instance()->getPointsDb($uid));

        return $res;

    }

    public static function removetrashPost($data)
    {
        global $wpdb;
        return $wpdb->query("DELETE FROM `" . $wpdb->base_prefix . self::DB_TABLE . "` WHERE type = 'new_post' AND data = '" . $data . "'");

    }

    public function get_participations($order_by = 'total', $order = 'DESC', $limit = null,$search=null)
    {

        global $wpdb;
        $order = strtoupper($order);
        $order = (in_array($order, array('ASC', 'DESC')) ? $order : 'DESC');

        $sql = ("SELECT DISTINCT(pp.uid),$wpdb->users.id,$wpdb->users.user_login,$wpdb->users.user_email, $wpdb->users.user_login, $wpdb->users.user_nicename,$wpdb->users.user_registered, m2.meta_value as total FROM " . $wpdb->base_prefix . self::DB_TABLE . " pp
                LEFT JOIN $wpdb->users on $wpdb->users.ID = pp.uid
                LEFT JOIN $wpdb->usermeta m1 on ($wpdb->users.ID = m1.user_id )
                LEFT JOIN $wpdb->usermeta m2 on ($wpdb->users.ID = m2.user_id AND m2.meta_key = 'pp_total' )
         WHERE 1 AND m1.meta_key = '{$wpdb->prefix}user_level'
         ".($search ? " AND wp_users.user_login LIKE '%$search%' OR wp_users.user_email LIKE '%$search%' " : '')."
         AND m1.meta_value = 0 ORDER BY " . $order_by . " " . $order . ($limit ? ' limit ' . $limit : ''));

        return $wpdb->get_results($sql, ARRAY_A);

    }


    public function get_points_details($user_id, $from = 0, $to = 10)
    {
        global $wpdb;
        $results = $wpdb->get_results('SELECT * FROM `' . $wpdb->base_prefix . self::DB_TABLE . '` ' . ' WHERE `uid` = ' . $user_id . ' ' . 'ORDER BY timestamp DESC LIMIT ' . $from . ',' . $to);
        $balance_results = $wpdb->get_results('SELECT * FROM `' . $wpdb->base_prefix . self::DB_TABLE . '` ' . ' WHERE `uid` = ' . $user_id . ' ' . 'ORDER BY timestamp DESC LIMIT ' . ($from) . ',18446744073709551615');

        $trads = array(
            'facebook_page_like' => 'Facebook Page Officiel',
            'facebook_post_like' => 'Facebook',
            'facebook_category_like' => 'Facebook',
            'google_plus' => 'Google +',
            'google_plus_category' => 'Google +',
            'dailypoints' => 'Daily Points',
            'facebook_comment' => 'Commentaire',
            'twitter_follow' => 'Twitter Follow',
            'twitter_tweet' => 'Twitter',
            'twitter_tweet_category' => 'Twitter',
            'email_points' => 'Email Points',
            'post_view' => 'Article visité',
            'share_contest' => 'Concours partagé',
            'attend_contest' => 'Participation au concours',
            'register_points' => 'Inscription'
        );


        $str = '<table class="tg">
        <tr>
            <th class="tg-date">date</th>
            <th class="tg-desc">description</th>
            <th class="tg-debut">POINTS</th>
            <th class="tg-fin">Balance</th>
        </tr>';




        $balance = 0;

        foreach ($balance_results as $bal) {
                $balance += $bal->points;
        }


        /*for ($i = 0; $i < count($results); $i++) {
            if($balance_results->id == $results[$i]->id){
                $balance += $value->points;

            }
        }*/
       /* foreach ($results as $value) {
            $balance += $value->points;

        }*/
        foreach ($results as $value) {

            if ($value->type != 'new_post') {

                $post_id = null;
                $thetitle = '--';
                $post_id = $value->data;

                $url = null;
                if ($post_id && ctype_digit($post_id)) {
                    $permalink = ($this->is_type_category($value->type) ? get_category_link($post_id) : get_permalink($post_id));
                    $thetitle = ($this->is_type_category($value->type) ? get_cat_name($post_id) : get_the_title($post_id));
                    $thetitle = mb_substr($thetitle, 0, 40);
                    $thetitle = (strlen($thetitle) > 30) ? $thetitle . "..." : $thetitle;
                    $url = '<a target="_blank" href="' . $permalink . '">' . $thetitle . '</a>';
                }

                $str .= '<tr>' .
                    '<td valign="middle">' . $this->pp_relativeTime($value->timestamp) . '</td>' .
                    '<td style="width:40px;" valign="middle">' . $trads[$value->type] . (($url) ? ' : ' . $url : '') . '</td >' .
                    '<td valign = "middle" > ' . $value->points . '</td >' .
                    '<td valign = "middle" > ' . $balance . '</td >' .
                    '</tr > ';

                $balance -= $value->points;
            }
        }

        $str .= '</table>';

        return $str;
    }

    public function is_type_category($type)
    {
        $category_type = array('google_plus_category', 'twitter_tweet_category', 'facebook_category_like');
        return in_array($type, $category_type);
    }

    public function get_results_count($user_id)
    {
        global $wpdb;
        return $wpdb->get_var('SELECT count(*) FROM `' . $wpdb->base_prefix . self::DB_TABLE . '` ' . ' WHERE `uid` = ' . $user_id);
    }


    public function get_points_details_admin($user_id)
    {
        global $wpdb;

        $type = $user_id;
        $limit = 10;
        $q = '';

        if ($type != 'all') {
            $uid = (int)$type;
            $q = ' WHERE `uid` = ' . $uid . ' AND type != "new_post" ';
        }
        if ($limit > 0) {
            $limitq = 'LIMIT ' . (int)$limit;
        }
        $results = $wpdb->get_results('SELECT * FROM `' . $wpdb->base_prefix . self::DB_TABLE . '` ' . $q . 'ORDER BY timestamp DESC ', ARRAY_A);

        return $results;


        return $str;
    }


    public function pp_relativeTime($timestamp)
    {
        $difference = time() - $timestamp;
        $periods = array(__('sec', 'cp'), __('min', 'cp'), __('heure', 'cp'), __('jour', 'cp'), __('semaine', 'cp'), __('mois', 'cp'), __('année', 'cp'), __('decade', 'cp'));
        $lengths = array("60", "60", "24", "7", "4.35", "12", "10");
        if ($difference >= 0) { // this was in the past
            $ending = __('Il y a', 'cp');
        } else { // this was in the future
            $difference = -$difference;
            $ending = __('to go', 'cp');
        }
        for ($j = 0; $difference >= $lengths[$j]; $j++)
            $difference /= $lengths[$j];
        $difference = round($difference);
        if ($difference != 1) $periods[$j] .= 's';
        $text = "$ending $difference $periods[$j]";
        return $text;
    }


    public function uninstall()
    {

        # Delete the table if you delete the plugin via the admin
        # don't delete the table if you just deactive the plugin
        # Delete the site_option "ubc_invitation_db_version"

    }

}


function statpp_relativeTimes($timestamp)
{
    $difference = time() - $timestamp;
    $periods = array(__('sec', 'cp'), __('min', 'cp'), __('heure', 'cp'), __('jour', 'cp'), __('semaine', 'cp'), __('mois', 'cp'), __('année', 'cp'), __('decade', 'cp'));
    $lengths = array("60", "60", "24", "7", "4.35", "12", "10");
    if ($difference >= 0) { // this was in the past
        $ending = __('Il y a', 'cp');
    } else { // this was in the future
        $difference = -$difference;
        $ending = __('to go', 'cp');
    }
    for ($j = 0; $difference >= $lengths[$j]; $j++)
        $difference /= $lengths[$j];
    $difference = round($difference);
    if ($difference != 1) $periods[$j] .= 's';
    $text = "$ending $difference $periods[$j]";
    return $text;
}


