<?php
/**
 * Invitation List Table used to display the invites table
 *
 *
 * @package   PIXPOINTS
 * @author    Pixweb médias | http://www.pixweb.ma/
 * @license   GPL-2.0+
 * @link      #
 * @copyright pixweb
 */
if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}


/************************** CREATE A PACKAGE CLASS *****************************
 *******************************************************************************
 * Create a new list table package that extends the core WP_List_Table class.
 * WP_List_Table contains most of the framework for generating the table, but we
 * need to define and override some methods so that our data can be displayed
 * exactly the way we need it to be.
 *
 * To display this example on a page, you will first need to instantiate the class,
 * then call $yourInstance->prepare_items() to handle any data manipulation, then
 * finally call $yourInstance->display() to render the table to the page.
 *
 * Our theme for this list table is going to be movies.
 */
class PIXPOINTS_User_List_Table extends WP_List_Table
{

    /** ************************************************************************
     * REQUIRED. Set up a constructor that references the parent constructor. We
     * use the parent reference to set some default configs.
     ***************************************************************************/
    function __construct()
    {
        global $status, $page;

        //Set parent defaults
        parent::__construct(array(
            'singular' => 'invitation', //singular name of the listed records
            'plural' => 'invitations', //plural name of the listed records
            'ajax' => false //does this table support ajax?
        ));

    }


    /** ************************************************************************
     * Recommended. This method is called when the parent class can't find a method
     * specifically build for a given column. Generally, it's recommended to include
     * one method for each column you want to render, keeping your package class
     * neat and organized. For example, if the class needs to process a column
     * named 'title', it would first see if a method named $this->column_title()
     * exists - if it does, that method will be used. If it doesn't, this one will
     * be used. Generally, you should try to use custom column methods as much as
     * possible.
     *
     * Since we have defined a column_title() method later on, this method doesn't
     * need to concern itself with any column with a name of 'title'. Instead, it
     * needs to handle everything else.
     *
     * For more detailed insight into how columns are handled, take a look at
     * WP_List_Table::single_row_columns()
     *
     * @param array $item A singular item (one full row's worth of data)
     * @param array $column_name The name/slug of the column to be processed
     * @return string Text or HTML to be placed inside the column <td>
     **************************************************************************/
    function column_default($item, $column_name)
    {

        switch ($column_name) {
            case 'role':
                return ucfirst($item[$column_name]);
                break;

            case 'status':
                return CTLT_Invitation_API::get_status($item[$column_name]);
                break;

            case 'inviter_id':
                $invited_by = get_user_by('id', $item['inviter_id']);
                return get_avatar($item['inviter_id'], 16) . " " . $invited_by->display_name;
                break;

            default:
                return print_r($item, true); //Show the whole array for troubleshooting purposes
                break;

        }
    }


    /** ************************************************************************
     * Recommended. This is a custom column method and is responsible for what
     * is rendered in any column with a name/slug of 'title'. Every time the class
     * needs to render a column, it first looks for a method named
     * column_{$column_title} - if it exists, that method is run. If it doesn't
     * exist, column_default() is called instead.
     *
     * This example also illustrates how to implement rollover actions. Actions
     * should be an associative array formatted as 'slug'=>'link html' - and you
     * will need to generate the URLs yourself. You could even ensure the links
     *
     *
     * @see WP_List_Table::::single_row_columns()
     * @param array $item A singular item (one full row's worth of data)
     * @return string Text to be placed inside the column <td> (movie title only)
     **************************************************************************/
    function column_points($item)
    {


        //Return the title contents
        return $item['points'];
    }

    function column_date($item)
    {
        $timestamp = $item['timestamp'];
        $actions = array(
            'timestamp' => date("M j, Y g:ia", $timestamp)
        );

        //Return the title contents
        return sprintf('%1$s',
            /*$1%s*/
            date("M j, Y g:ia", $timestamp)
        );
    }

    /** ************************************************************************
     * REQUIRED if displaying checkboxes or using bulk actions! The 'cb' column
     * is given special treatment when columns are processed. It ALWAYS needs to
     * have it's own method.
     *
     * @see WP_List_Table::::single_row_columns()
     * @param array $item A singular item (one full row's worth of data)
     * @return string Text to be placed inside the column <td> (movie title only)
     **************************************************************************/
    function column_type($item)
    {

        $trads = array(
            'facebook_page_like' => 'Facebook Page Officiel',
            'facebook_post_like' => 'Facebook',
            'facebook_category_like' => 'Facebook Category',
            'google_plus' => 'Google +',
            'google_plus_category' => 'Google +',
            'dailypoints' => 'Daily Points',
            'facebook_comment' => 'Commentaire',
            'twitter_follow' => 'Twitter Follow',
            'twitter_tweet' => 'Twitter tweet',
            'twitter_tweet_category' => 'Twitter',
            'email_points' => 'Email Points',
            'post_view' => 'Article visité',
            'share_contest' => 'Concours partagé',
            'attend_contest' => 'Participation au concours',
            'register_points' => 'Inscription'
        );

        $str = '';

        $thetitle = '--';
        $post_id = $item['data'];

        $url = null;
        if ($post_id && ctype_digit($post_id)) {
            $permalink = ($this->is_type_category($item['type']) ? get_category_link($post_id) : get_permalink($post_id));
            $thetitle = ($this->is_type_category($item['type']) ? get_cat_name($post_id) : get_the_title($post_id));
            $thetitle = mb_substr($thetitle, 0, 40);
            $thetitle = (strlen($thetitle) > 30) ? $thetitle . "..." : $thetitle;
            $url = '<a target="_blank" href="' . $permalink . '">' . $thetitle . '</a>';
        }


        $str .= $trads[$item['type']] . (($url) ? ' : ' . $url : '') . '</td >';


        return $str;
    }

    public function is_type_category($type)
    {
        $category_type = array('google_plus_category', 'twitter_tweet_category', 'facebook_category_like');
        return in_array($type, $category_type);
    }

    public function get_results_count($user_id)
    {
        global $wpdb;
        return $wpdb->get_var('SELECT count(*) FROM `' . $wpdb->base_prefix . self::DB_TABLE . '` ' . ' WHERE `uid` = ' . $user_id);
    }


    /** ************************************************************************
     * REQUIRED! This method dictates the table's columns and titles. This should
     * return an array where the key is the column slug (and class) and the value
     * is the column's title text. If you need a checkbox for bulk actions, refer
     * to the $columns array below.
     *
     * The 'cb' column is treated differently than the rest. If including a checkbox
     * column in your table you must create a column_cb() method. If you don't need
     * bulk actions or checkboxes, simply leave the 'cb' entry out of your array.
     *
     * @see WP_List_Table::::single_row_columns()
     * @return array An associative array containing column information: 'slugs'=>'Visible Titles'
     **************************************************************************/
    function get_columns()
    {
        $columns = array(
            'type' => 'Type',
            'date' => 'Date',
            'points' => 'Points',
        );
        return $columns;
    }

    /** ************************************************************************
     * Optional. If you want one or more columns to be sortable (ASC/DESC toggle),
     * you will need to register it here. This should return an array where the
     * key is the column that needs to be sortable, and the value is db column to
     * sort by. Often, the key and value will be the same, but this is not always
     * the case (as the value is a column name from the database, not the list table).
     *
     * This method merely defines which columns should be sortable and makes them
     * clickable - it does not handle the actual sorting. You still need to detect
     * the ORDERBY and ORDER querystring variables within prepare_items() and sort
     * your data accordingly (usually by modifying your query).
     *
     * @return array An associative array containing all the columns that should be sortable: 'slugs'=>array('data_values',bool)
     **************************************************************************/
    function get_sortable_columns()
    {
        $sortable_columns = array( //            'date' => array('date', false), //true means it's already sorted
        );
        return $sortable_columns;
    }


    /** ************************************************************************
     * REQUIRED! This is where you prepare your data for display. This method will
     * usually be used to query the database, sort and filter the data, and generally
     * get it ready to be displayed. At a minimum, we should set $this->items and
     * $this->set_pagination_args(), although the following properties and methods
     * are frequently interacted with here...
     *
     * @global WPDB $wpdb
     * @uses $this->_column_headers
     * @uses $this->items
     * @uses $this->get_columns()
     * @uses $this->get_sortable_columns()
     * @uses $this->get_pagenum()
     * @uses $this->set_pagination_args()
     **************************************************************************/
    function prepare_items()
    {
        global $wpdb; //This is used only if making any database queries

        /**
         * First, lets decide how many records per page to show
         */
        $per_page = 20;


        /**
         * REQUIRED. Now we need to define our column headers. This includes a complete
         * array of columns to be displayed (slugs & titles), a list of columns
         * to keep hidden, and a list of columns that are sortable. Each of these
         * can be defined in another method (as we've done here) before being
         * used to build the value for our _column_headers property.
         */
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();


        /**
         * REQUIRED. Finally, we build an array to be used by the class for column
         * headers. The $this->_column_headers property takes an array which contains
         * 3 other arrays. One for all columns, one for hidden columns, and one
         * for sortable columns.
         */
        $this->_column_headers = array($columns, $hidden, $sortable);

        /**
         * Instead of querying a database, we're going to fetch the example data
         * property we created for use in this plugin. This makes this example
         * package slightly different than one you might build on your own. In
         * this example, we'll be using array manipulation to sort and paginate
         * our data. In a real-world implementation, you will probably want to
         * use sort and pagination data to build a custom query instead, as you'll
         * be able to use your precisely-queried data immediately.
         */

        $pixpoints_api = PIXPOINTS_API::get_instance();
        $data = $pixpoints_api->get_points_details_admin($_GET['id']);

        /**
         * REQUIRED for pagination. Let's figure out what page the user is currently
         * looking at. We'll need this later, so you should always include it in
         * your own package classes.
         */
        $current_page = $this->get_pagenum();

        /**
         * REQUIRED for pagination. Let's check how many items are in our data array.
         * In real-world use, this would be the total number of items in your database,
         * without filtering. We'll need this later, so you should always include it
         * in your own package classes.
         */
        $total_items = count($data);


        /**
         * The WP_List_Table class does not handle pagination for us, so we need
         * to ensure that the data is trimmed to only the current page. We can use
         * array_slice() to
         */
        $data = array_slice($data, (($current_page - 1) * $per_page), $per_page);


        /**
         * REQUIRED. Now we can add our *sorted* data to the items property, where
         * it can be used by the rest of the class.
         */
        $this->items = $data;


        /**
         * REQUIRED. We also have to register our pagination options & calculations.
         */
        $this->set_pagination_args(array(
            'total_items' => $total_items, //WE have to calculate the total number of items
            'per_page' => $per_page, //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items / $per_page) //WE have to calculate the total number of pages
        ));
    }

}





