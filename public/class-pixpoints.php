<?php
/**
 * Plugin Name.
 *
 * @package   PixPoints
 * @author    Pixweb médias <http://www.pixweb.ma/>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 Pixweb
 */

/**
 * Plugin class. This class should ideally be used to work with the
 * public-facing side of the WordPress site.
 *
 * If you're interested in introducing administrative or dashboard
 * functionality, then refer to `class-pixpoints-admin.php`
 *
 * @TODO: Rename this class to a proper name for your plugin.
 *
 * @package PixPoints
 * @author  Pixweb médias <http://www.pixweb.ma/>
 */
class PixPoints
{

    /**
     * Plugin version, used for cache-busting of style and script file references.
     *
     * @since   1.0.0
     *
     * @var     string
     */
    const VERSION = '1.0.0';

    /**
     * @TODO - Rename "pixpoints" to the name your your plugin
     *
     * Unique identifier for your plugin.
     *
     *
     * The variable name is used as the text domain when internationalizing strings
     * of text. Its value should match the Text Domain file header in the main
     * plugin file.
     *
     * @since    1.0.0
     *
     * @var      string
     */
    protected $plugin_slug = 'pixpoints';

    /**
     * Instance of this class.
     *
     * @since    1.0.0
     *
     * @var      object
     */
    protected static $instance = null;

    /**
     * Initialize the plugin by setting localization and loading public scripts
     * and styles.
     *
     * @since     1.0.0
     */
    private function __construct()
    {


        // Load plugin text domain
        add_action('init', array($this, 'load_plugin_textdomain'));
        add_action('wp_head', array($this, 'get_total_points_init'));
        add_action( 'init', array($this,'pixweb_updater_init'));

        // Activate plugin when new blog is added
        add_action('wpmu_new_blog', array($this, 'activate_new_site'));

        // Load public-facing style sheet and JavaScript.
        add_action('wp_enqueue_scripts', array($this, 'enqueue_styles'));
        add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));


        add_action('wp_head', array($this, 'pp_social_script'));


        add_action('add_points_direct', array($this, 'add_points_direct'), 10, 3);


        /*
         * Ajax Functions
         */
        add_action('wp_ajax_add_points', array($this, 'add_points'));
        add_action('wp_ajax_profil_points', array($this, 'profil_points'), 10, 1);
        add_action('wp_ajax_remove_points', array($this, 'remove_points'));
        add_action('wp_ajax_total_points', array($this, 'total_points'));
        add_action('wp_ajax_get_plusones', array($this, 'get_plusones'), 10, 1);
        add_action('wp_head', array($this, 'post_views_points'));

        add_shortcode('pixpoints', array($this, 'pixpoints_shortcode'));

        add_action('save_post', array($this, 'attributes_save_postdata'));

    }


    public function pixweb_updater_init(){


        if ( is_admin() ) { // note the use of is_admin() to double check that this is happening in the admin
            if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
                if ( ! class_exists( 'GitHub_Updater' ) ) {
                    require_once PIXPOINTS_PLUGIN_PATH.'includes/class-github-updater.php';
                    require_once PIXPOINTS_PLUGIN_PATH.'includes/class-github-api.php';
                    require_once PIXPOINTS_PLUGIN_PATH.'includes/class-bitbucket-api.php';
                }
                if ( ! class_exists( 'GitHub_Plugin_Updater' ) ) {
                    require_once PIXPOINTS_PLUGIN_PATH.'includes/class-plugin-updater.php';
                    new GitHub_Plugin_Updater;
                }
                if ( ! class_exists( 'GitHub_Theme_Updater' ) ) {
                    require_once PIXPOINTS_PLUGIN_PATH.'includes/class-theme-updater.php';
                    new GitHub_Theme_Updater;
                }
            }
        }

    }
    public function attributes_save_postdata($post_id)
    {

        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
        $post = get_post($post_id);
        $str = json_encode($post);
        $uid = $post->post_author;
        if ($post->post_type == 'post') {
            if ('publish' == $post->post_status) {
                if (class_exists('PIXPOINTS_API')) {
                    PIXPOINTS_API::get_instance()->log('new_post', $uid, 0, $post_id);
                }
            } elseif ('trash' == $post->post_status) {
                if (class_exists('PIXPOINTS_API')) {
                    PIXPOINTS_API::get_instance()->removetrashPost($post_id);
                }
            }
        }
    }


    public function pixpoints_shortcode($attr, $content = null, $tag = 'pixpoints')
    {


        $defaults = array(
            'type' => false,
            'url' => false,
            'status' => false,
            'msg' => false,
            'field' => false,
            'id' => false
        );

        extract(shortcode_atts($defaults, $attr, $tag));

        $content = '';
        if ($type == 'page') {
            ob_start();
            include(PIXPOINTS_PLUGIN_PATH . 'public/views/leaderboard-page.php');
            $content .= ob_get_clean();

        } elseif ($type == 'profile') {
            ob_start();
            include(PIXPOINTS_PLUGIN_PATH . 'public/views/profil-page.php');
            $content .= ob_get_clean();

        }


        return $content;

    }


    public function post_views_points()
    {
        if (!is_single() || !is_user_logged_in())
            return;

        if (empty ($post_id)) {
            global $post;
            $post_id = $post->ID;
        }

        $pp_enable_post_view = get_option('pp_enable_post_view');
        $pp_post_view_points = get_option('pp_post_view_points');

        if ($pp_enable_post_view !== 0) {

            $post_date = strtotime($post->post_date);
            $date_month_ago = strtotime(date("Y-m-d", strtotime("-1 month")));

            if ($post_date < $date_month_ago) return;

            $pixpoints_api = PIXPOINTS_API::get_instance();
            $user_id = get_current_user_id();
            $getlog = $pixpoints_api->getPointsDb($user_id, 'post_view', $post_id);
            if ($getlog == 0) {
                $pixpoints_api->log('post_view', $user_id, $pp_post_view_points, $post_id);
            }
        }

    }

    public static function get_total_points_init()
    {
        $user = wp_get_current_user();
        $pixpoints_api = PIXPOINTS_API::get_instance();


        $user_total_points = $pixpoints_api->getPointsDb($user->ID);
        if ($user_total_points < 0)
            $user_total_points = 0;

        return $user_total_points;
    }


    public function add_points_direct($type, $uid, $data = null)
    {

        $pixpoints_api = PIXPOINTS_API::get_instance();
        $pixpoints_api->log($type, $uid, get_option($this->getOption($type)), $data);


    }

    public function get_plusones()
    {

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://clients6.google.com/rpc");
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"' . $_GET['url'] . '","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        $curl_results = curl_exec($curl);
        curl_close($curl);
        $json = json_decode($curl_results, true);
        echo intval($json[0]['result']['metadata']['globalCounts']['count']);
        die();
    }


    public function profil_points()
    {
        $pixpoints_api = PIXPOINTS_API::get_instance();
        echo $pixpoints_api->get_points_details(get_current_user_id(), $_GET['data']);
        die();
    }


    /**
     * Return the plugin slug.
     *
     * @since    1.0.0
     *
     * @return    Plugin slug variable.
     */
    public function get_plugin_slug()
    {
        return $this->plugin_slug;
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance()
    {

        // If the single instance hasn't been set, set it now.
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Fired when the plugin is activated.
     *
     * @since    1.0.0
     *
     * @param    boolean $network_wide True if WPMU superadmin uses
     *                                       "Network Activate" action, false if
     *                                       WPMU is disabled or plugin is
     *                                       activated on an individual blog.
     */
    public static function activate($network_wide)
    {

        if (function_exists('is_multisite') && is_multisite()) {

            if ($network_wide) {

                // Get all blog ids
                $blog_ids = self::get_blog_ids();

                foreach ($blog_ids as $blog_id) {

                    switch_to_blog($blog_id);
                    self::single_activate();
                }

                restore_current_blog();

            } else {
                self::single_activate();
            }

        } else {
            self::single_activate();
        }

    }

    /**
     * Fired when the plugin is deactivated.
     *
     * @since    1.0.0
     *
     * @param    boolean $network_wide True if WPMU superadmin uses
     *                                       "Network Deactivate" action, false if
     *                                       WPMU is disabled or plugin is
     *                                       deactivated on an individual blog.
     */
    public static function deactivate($network_wide)
    {

        if (function_exists('is_multisite') && is_multisite()) {

            if ($network_wide) {

                // Get all blog ids
                $blog_ids = self::get_blog_ids();

                foreach ($blog_ids as $blog_id) {

                    switch_to_blog($blog_id);
                    self::single_deactivate();

                }

                restore_current_blog();

            } else {
                self::single_deactivate();
            }

        } else {
            self::single_deactivate();
        }

    }

    /**
     * Fired when a new site is activated with a WPMU environment.
     *
     * @since    1.0.0
     *
     * @param    int $blog_id ID of the new blog.
     */
    public function activate_new_site($blog_id)
    {

        if (1 !== did_action('wpmu_new_blog')) {
            return;
        }

        switch_to_blog($blog_id);
        self::single_activate();
        restore_current_blog();

    }

    /**
     * Get all blog ids of blogs in the current network that are:
     * - not archived
     * - not spam
     * - not deleted
     *
     * @since    1.0.0
     *
     * @return   array|false    The blog ids, false if no matches.
     */
    private static function get_blog_ids()
    {

        global $wpdb;

        // get an array of blog ids
        $sql = "SELECT blog_id FROM $wpdb->blogs
			WHERE archived = '0' AND spam = '0'
			AND deleted = '0'";

        return $wpdb->get_col($sql);

    }

    /**
     * Fired for each blog when the plugin is activated.
     *
     * @since    1.0.0
     */
    private static function single_activate()
    {
        // @TODO: Define activation functionality here
    }

    /**
     * Fired for each blog when the plugin is deactivated.
     *
     * @since    1.0.0
     */
    private static function single_deactivate()
    {
        // @TODO: Define deactivation functionality here
    }

    /**
     * Load the plugin text domain for translation.
     *
     * @since    1.0.0
     */
    public function load_plugin_textdomain()
    {

        $domain = $this->plugin_slug;
        $locale = apply_filters('plugin_locale', get_locale(), $domain);

        load_textdomain($domain, trailingslashit(WP_LANG_DIR) . $domain . '/' . $domain . '-' . $locale . '.mo');
        load_plugin_textdomain($domain, FALSE, basename(plugin_dir_path(dirname(__FILE__))) . '/languages/');

    }

    /**
     * Register and enqueue public-facing style sheet.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {
        wp_enqueue_style($this->plugin_slug . '-bootstrap.css', plugins_url('assets/css/bootstrap.css', __FILE__), array(), self::VERSION);
        wp_enqueue_style($this->plugin_slug . '-plugin-styles', plugins_url('assets/css/public.css', __FILE__), array(), self::VERSION);

        // notify
        wp_enqueue_style('toatsr', plugins_url('assets/css/toastr.css', __FILE__));


    }

    /**
     * Register and enqueues public-facing JavaScript files.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {
        wp_enqueue_script($this->plugin_slug . '-plugin-script', plugins_url('assets/js/public.js', __FILE__), array('jquery'), self::VERSION);
        wp_enqueue_script('bootstrap.js', plugins_url('assets/js/bootstrap.min.js', __FILE__), array('jquery'), self::VERSION);
        wp_enqueue_script('countUp.js', plugins_url('assets/js/countUp.js', __FILE__), array('jquery'), self::VERSION);

        wp_localize_script('jquery', 'points_ajax_script', array('ajax_url' => admin_url('admin-ajax.php')));
        wp_register_script('twitterwidgets', ('//platform.twitter.com/widgets.js'), false, null, true);

        // notify
        wp_register_script('twitterwidgets', ('//platform.twitter.com/widgets.js'), false, null, true);

        wp_enqueue_script('toatsr', plugins_url('assets/js/toastr.js', __FILE__), array('jquery'), '1.0.1');


    }


    public function pp_social_script()
    {


        echo "<script>";

        echo "var show_popup = " . get_option('pp_enable_popup') . ";";
        if (get_option('pp_enable_like_page') === 'on') {

            echo "var pp_like_page_points = " . get_option('pp_like_page_points') . ";";
            echo "var pp_text_like_page = '" . get_option('pp_text_like_page') . "';";

        }
        if (get_option('pp_enable_like_post') === 'on') {

            echo "var pp_like_post_points =" . get_option('pp_like_post_points') . ";";
            echo "var pp_text_like_post = '" . get_option('pp_text_like_post') . "';";

        }
        if (get_option('pp_enable_comment') === 'on') {
            echo "var pp_comment_points =" . get_option('pp_comment_points') . ";";
            echo "var pp_text_popup = '" . get_option('pp_text_popup') . "';";

        }
        if (get_option('pp_enable_follow') === 'on') {
            echo "var pp_follow_points =" . get_option('pp_follow_points') . ";";
            echo "var pp_text_follow = '" . get_option('pp_text_follow') . "';";

        }

        if (get_option('pp_enable_tweet') === 'on') {
            echo "var pp_tweet_points =" . get_option('pp_tweet_points') . ";";
            echo "var pp_text_tweet = '" . get_option('pp_text_tweet') . "';";

        }
        if (get_option('pp_enable_google') === 'on') {
            echo "var pp_google_points =" . get_option('pp_google_points') . ";";
            echo "var pp_text_google = '" . get_option('pp_text_google') . "';";

        }

        echo "var current_page = '" . get_query_var('pagename') . "';";

        echo "var is_category = " . (json_encode(is_category())) . ";";
        echo "var post_id = " . ((is_category()) ? get_query_var('cat') : get_the_ID()) . ";";

        echo "</script>";
    }


    public function add_points($uid = null, $type = null, $data = null)
    {


        if (!$uid) {
            $user = wp_get_current_user();
            $uid = wp_get_current_user()->ID;
        }

        $pixpoints_api = PIXPOINTS_API::get_instance();


        $pp_day_total = (int)$pixpoints_api->getPoints24Hours($uid);
        $pp_max = (int)get_option('pp_max_day_points');
        if ($pp_day_total > $pp_max && $type != 'register_points') {
            return false;
        }




        (isset($_POST['type'])) ? $type = $_POST['type'] : null;
        (isset($_POST['data'])) ? $data = $_POST['data'] : null;


        if ($type == null)
            return false;

        $exec = $pixpoints_api->log($type, $uid, get_option($this->getOption($type)), $data);
        if ($exec)
            echo(get_option($this->getOption($_POST['type'])));

        die;
    }


    public function total_points()
    {
        $user = wp_get_current_user();
        $pixpoints_api = PIXPOINTS_API::get_instance();


        $exec = $pixpoints_api->getPointsDb($user->ID);
        if ($exec < 0)
            $exec = 0;


        $points = update_user_meta($user->ID, 'ppoints', $exec);
        echo($exec);

        die;
    }

    public function remove_points()
    {
        $user = wp_get_current_user();
        if (!isset($_POST['type'])) {
            return false;
        }

        (isset($_POST['data'])) ? $data = $_POST['data'] : null;


        $pixpoints_api = PIXPOINTS_API::get_instance();
        $pixpoints_api->removePoints($_POST['type'], $user->ID, $data);
        die;
    }


    public function getOption($type)
    {

        $option = '';

        if ($type == 'facebook_page_like') {
            $option = 'pp_like_page_points';
        } elseif ($type == 'facebook_post_like') {
            $option = 'pp_like_post_points';
        } elseif ($type == 'facebook_category_like') {
            $option = 'pp_like_post_points';
        } elseif ($type == 'facebook_comment') {
            $option = 'pp_comment_points';
        } elseif ($type == 'facebook_share') {
            $option = 'pp_share_points';
        } elseif ($type == 'twitter_follow') {
            $option = 'pp_follow_points';
        } elseif ($type == 'twitter_tweet') {
            $option = 'pp_tweet_points';
        } elseif ($type == 'twitter_tweet_category') {
            $option = 'pp_tweet_points';
        } elseif ($type == 'google_plus') {
            $option = 'pp_google_points';
        } elseif ($type == 'google_plus_category') {
            $option = 'pp_google_points';
        } elseif ($type == 'email_points') {
            $option = 'pp_email_points';

        } elseif ($type == 'register_points') {
            $option = 'pp_register_points';

        } elseif ($type == 'attend_contest') {
            $option = 'pp_attend_contest_points';

        } elseif ($type == 'share_contest') {
            $option = 'pp_share_contest_points';

        } else
            return false;

        return $option;

    }


    public function get_user_points()
    {

    }


}
