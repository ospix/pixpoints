<?php
/**
 * Created by PhpStorm.
 * User: Pixweb médias
 * Date: 04/06/14
 * Time: 15:30
 */

$pixpoints_api = PIXPOINTS_API::get_instance();


$results_count = $pixpoints_api->get_results_count(get_current_user_id());
$item_per_page = 10;
$pages = ceil($results_count / $item_per_page);
$from = 0;
$to = 10;
$details = $pixpoints_api->get_points_details(wp_get_current_user()->ID, $from, $to);


?>

<div class="spinner">
    <div class="bounce1"></div>
    <div class="bounce2"></div>
    <div class="bounce3"></div>
</div>
<div class="js-profil-details">


    <div class="js-profil-table">
        <?php echo $details; ?>

    </div>


    <div class="pagination">
        <?php
        for ($i = 0; $i < $pages; $i++) {

            echo '<a class="page-numbers' . (($i == 0) ? ' current' : '') . '" data-n="' . ($i * $item_per_page) . '" href="#' . ($i + 1) . '">' . ($i + 1) . '</a>';
        }
        ?>
    </div>


</div>


<script>

    $(function () {
        $('.js-profil-details .pagination a').click(function () {

            $('.js-profil-details .js-profil-table').animate({opacity: 0.5}, 300, function () {

                $('.spinner').show();
            });

            $('.js-profil-details .pagination a').removeClass('current');
            $(this).addClass('current');
            $.get(points_ajax_script.ajax_url, {action: 'profil_points', data: $(this).data('n')}, function (data) {
                $('.js-profil-table').html(data);
                $('.js-profil-details .js-profil-table').animate({opacity: 1});
                $('.spinner').hide();

            });

            return false;
        });

    })


</script>


<style>
    .current {
        pointer-events: none;
        cursor: default;
        color: #000;
    }

    .spinner {
        margin: 100px auto 0;
        width: 70px;
        text-align: center;
        position: absolute;
        left: 40%;
        top: 14%;
        display: none;
        z-index: 9999;
    }

    .spinner > div {
        width: 18px;
        height: 18px;
        background-color: #1c5c87;

        border-radius: 100%;
        display: inline-block;
        -webkit-animation: bouncedelay 1.4s infinite ease-in-out;
        animation: bouncedelay 1.4s infinite ease-in-out;
        /* Prevent first frame from flickering when animation starts */
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
    }

    .spinner .bounce1 {
        -webkit-animation-delay: -0.32s;
        animation-delay: -0.32s;
    }

    .spinner .bounce2 {
        -webkit-animation-delay: -0.16s;
        animation-delay: -0.16s;
    }

    @-webkit-keyframes bouncedelay {
        0%, 80%, 100% {
            -webkit-transform: scale(0.0)
        }
        40% {
            -webkit-transform: scale(1.0)
        }
    }

    @keyframes bouncedelay {
        0%, 80%, 100% {
            transform: scale(0.0);
            -webkit-transform: scale(0.0);
        }
        40% {
            transform: scale(1.0);
            -webkit-transform: scale(1.0);
        }
    }


</style>