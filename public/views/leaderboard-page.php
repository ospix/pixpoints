<?php
/**
 * Created by PhpStorm.
 * User: Pixweb médias
 * Date: 04/06/14
 * Time: 15:30
 */


$pixpoints_api = PIXPOINTS_API::get_instance();


$data = $pixpoints_api->get_participations('total', 'DESC', 20);

?>

<ul>

<?php foreach ($data as $key => $item): ?>


    <?php

    $fb_avatar = get_user_meta($item['uid'], 'facebook_avatar');

    $name = get_user_meta($item['uid'],'first_name',true);
    if(trim($name) == ''){
        $name = get_user_meta($item['uid'],'nickname',true);
    }

    $img = '';
    if ($fb_avatar)
        $img = "<div style='float: left; margin-right: 10px'><img  width='40px' height='40px' src='{$fb_avatar[0]}' /></div>";
    else
        $img = "<div style='float: left; margin-right: 10px'>" . get_avatar($item['user_login'], '40') . "</div>";
    $img .= '<strong>' . $name . '</strong>';
    $img .= ' inscrit(e) depuis le ' . date('d/m/Y', strtotime($item['user_registered'])) . '<br/>';

    ?>
    <li class="row" style="height: 60px;">
        <?php echo $img; ?>
        <div class="col-md pointz"><?php echo $item['total']; ?> points</div>
    </li>


<?php endforeach; ?>


</ul>

