(function ($) {
    "use strict";

    $(function () {
        get_total_points();

        if (show_popup != null && show_popup == 1)
            pp_social_script();
        initGoogleSdk();
        setTimeout(function () {
            initFacebookSdk();
            initTwitterSdk();
//            if (typeof permalink != 'undefined')
//                getTotalSocialCount();
        }, 5000);
    });


    function pp_social_script() {
        if (typeof pp_like_page_points !== 'undefined')
            $('.js-fb-like-page').popover(getOptions(pp_like_page_points, pp_text_like_page));

        if (typeof pp_like_post_points !== 'undefined')
            $('.js-fb-like-post').popover(getOptions(pp_like_post_points, pp_text_like_post));

        if (typeof pp_follow_points !== 'undefined')
            $('.js-twitter-follow-cp').popover(getOptions(pp_follow_points, pp_text_follow));

        if (typeof pp_tweet_points !== 'undefined')
            $('.js-twitter-tweet-cp').popover(getOptions(pp_tweet_points, pp_text_tweet));

        if (typeof pp_google_points !== 'undefined')
            $('.js-google-cp').popover(getOptions(pp_google_points, pp_text_google));
    }


    function getOptions(points, str) {
        return {
            html: true,
            placement: 'bottom',
            content: function () {
                return  str.replace(/pp_points/gi, points)
            },
            trigger: 'hover'

        };
    }


    function initGoogleSdk() {
        window.___gcfg = {lang: 'fr'};

        (function () {
            var po = document.createElement('script');
            po.type = 'text/javascript';
            po.async = true;
            po.src = 'https://apis.google.com/js/platform.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(po, s);
        })();


    }


    /**
     *
     * Callbacks Twitter
     *
     */


    function initTwitterSdk() {


        if (typeof twttr !== undefined) {
            twttr.ready(function (twttr) {

                if (typeof pp_follow_points !== 'undefined') {
                    twttr.events.bind('follow', function (event) {
                        $.post(points_ajax_script.ajax_url, {action: 'add_points', type: 'twitter_follow', data: 'Follow'}, function (data) {
                            add_notice(data);
                            get_total_points();
                        });
                    });


                    twttr.events.bind('unfollow', function (event) {
                        $.post(points_ajax_script.ajax_url, {action: 'remove_points', type: 'twitter_follow', data: 'Follow'}, function (data) {
                            get_total_points();

                        });

                    });
                }
                if (typeof pp_tweet_points !== 'undefined') {


                    twttr.events.bind('tweet', function (event) {
                        var type = (true == is_category) ? 'twitter_tweet_category' : 'twitter_tweet';


                        $.post(points_ajax_script.ajax_url, {action: 'add_points', type: type, data: post_id}, function (data) {
                            add_notice(data);
                            get_total_points();
                            update_share_count(1);


                        });
                    });
                }

            });
        }


    }


    /**
     *
     * Callbacks Facebook
     *
     */
    function initFacebookSdk() {
        if (typeof pp_like_page_points !== 'undefined' || typeof pp_like_post_points !== 'undefined') {
            FB.Event.subscribe('edge.create', like_callback);
            FB.Event.subscribe('edge.remove', unlike_callback);
        }

        if (typeof pp_comment_points !== 'undefined') {
            FB.Event.subscribe('comment.create', comment_callback);
            FB.Event.subscribe('comment.remove', uncomment_callback);
            FB.Event.subscribe('message.send', message_send_callback);

        }

    }


    function message_send_callback(response) {
        $.post(points_ajax_script.ajax_url, {action: 'add_points', type: 'facebook_share'}, function (data) {
            add_notice(data);
            get_total_points();

        });


    }

    function comment_callback(response) {
        $.post(points_ajax_script.ajax_url, {action: 'add_points', type: 'facebook_comment', data: (post_id + '_' + response.commentID)}, function (data) {
            add_notice(data);
            get_total_points();

        });

    }

    function uncomment_callback(response) {

        $.post(points_ajax_script.ajax_url, {action: 'remove_points', type: 'facebook_comment', data: (post_id + '_' + response.commentID)}, function () {
            get_total_points();
        });

    }


    function unlike_callback(url, html_element) {


        var type = '';
        var data = '';

        if (url.indexOf('https://www.facebook.com') >= 0) {

            type = 'facebook_page_like';
            data = 'J\'aime Fan Page';

        } else {

            type = (true == is_category) ? 'facebook_category_like' : 'facebook_post_like';
            data = post_id;
        }

        $.post(points_ajax_script.ajax_url, {action: 'remove_points', type: type, data: data}, function () {
            get_total_points();
            update_share_count(-1);

        });


    }


    function like_callback(url, html_element) {

        var type = '';
        var data = '';
        if (url.indexOf('https://www.facebook.com') >= 0) {

            type = 'facebook_page_like';
            data = 'J\'aime Fan Page';

        } else {
            type = (true == is_category) ? 'facebook_category_like' : 'facebook_post_like';
            data = post_id;
        }


        $.post(points_ajax_script.ajax_url, {action: 'add_points', type: type, data: data}, function (data) {
            add_notice(data);
            get_total_points();
            update_share_count(1);
        });

    }


}(jQuery));

function getTotalSocialCount() {

    permalink = 'http://www.quebecechantillonsgratuits.com/quebec-concours/15-cartes-cadeaux-100-subway-gagner-chaque-semaine-grand-prix-1000.html';
    $.getJSON("http://urls.api.twitter.com/1/urls/count.json?url=" + permalink + "&callback=?", function (json) {
        update_share_count(json.count);

    });

    $.getJSON("http://api.pinterest.com/v1/urls/count.json?callback%20&url=" + permalink + "&callback=?", function (data) {
        update_share_count(data.count);
    });


    $.getJSON(points_ajax_script.ajax_url, {"action": "get_plusones", "url": permalink}, function (data) {
        update_share_count(data);
    });

    /*$.get('https://api.facebook.com/method/fql.query?query=select%20%20like_count%20from%20link_stat%20where%20url="' + permalink + '"', function (data) {
     update_share_count($(data).find('like_count').text());
     });
     */

    $.get('https://api.facebook.com/method/fql.query?query=SELECT%20like_info.like_count,%20comment_info.comment_count,%20share_count%20from%20link_stat%20where%20url="' + permalink + '"', function (data) {
        update_share_count($(data).find('like_count').text());
    });
    // grab from facebook


}

function update_share_count(count) {
    var current_count = parseInt($('.st_sharethis_hcount').text());
    var counts = current_count + parseInt(count);
    if (counts < 0)
        counts = 0;
    $('.st_sharethis_hcount').text(counts);

}


function google_plus_callback(response) {

    if (typeof pp_google_points !== 'undefined') {
        var data = response.href;
        if (response.href == 'http://www.quebecechantillonsgratuits.com/')
            data = '+1 Notre site';
        else
            data = post_id;


        var type = (true == is_category) ? 'google_plus_category' : 'google_plus';


        if (response.state == "on") {
            $.post(points_ajax_script.ajax_url, {action: 'add_points', type: type, data: data}, function (data) {
                add_notice(data);
                get_total_points();
            });

        } else if (response.state == "off") {
            $.post(points_ajax_script.ajax_url, {action: 'remove_points', type: type, data: data}, function (data) {
                get_total_points();
            });

        }

    }
}


function add_notice(data) {

    if (data == 0) {
        return false;
    }
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "30000000",
        "hideDuration": "30000000",
        "timeOut": "30000000",
        "extendedTimeOut": "30000000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    toastr.success('+' + data);


}


function get_total_points() {
    $.get(points_ajax_script.ajax_url, {action: 'total_points'}, function (data) {
        $('.update_point_anywhere').text(data);
    });
}





