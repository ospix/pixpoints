<?php
/**
 * The WordPress Plugin Boilerplate.
 *
 * A foundation off of which to build well-documented WordPress plugins that
 * also follow WordPress Coding Standards and PHP best practices.
 *
 * @package   PixPoints
 * @author    Pixweb médias <http://www.pixweb.ma/>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 Pixweb
 *
 * @wordpress-plugin
 * Plugin Name:       pixpoints
 * Plugin URI:        #
 * Description:       @TODO
 * Version:           3.0.0
 * Author:            @TODO
 * Author URI:        Pixweb médias <http://www.pixweb.ma/>
 * Text Domain:       pixweb
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 * Bitbucket Plugin URI: https://bitbucket.org/ospix/pixpoints
 * Bitbucket Branch:     master
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/*----------------------------------------------------------------------------*
 * Public-Facing Functionality
 *----------------------------------------------------------------------------*/

define( 'PIXPOINTS_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

require_once( PIXPOINTS_PLUGIN_PATH . 'includes/class-pixpoints-api.php' );


require_once( plugin_dir_path( __FILE__ ) . 'public/class-pixpoints.php' );

require_once( PIXPOINTS_PLUGIN_PATH . 'widgets/widget-leaderboard.php' );


//PUSHER

define('APP_ID', '79049');
define('APP_KEY', '1938f98118ef52e38b38');
define('APP_SECRET', 'de6ffc2a98d86c1d50e4');

require(PIXPOINTS_PLUGIN_PATH . 'pusher-php-server-master/lib/Pusher.php');



/*
 * Register hooks that are fired when the plugin is activated or deactivated.
 * When the plugin is deleted, the uninstall.php file is loaded.
 */
register_activation_hook( __FILE__, array( 'PixPoints', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'PixPoints', 'deactivate' ) );

add_action( 'plugins_loaded', array( 'PixPoints', 'get_instance' ) );

/*----------------------------------------------------------------------------*
 * Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------*/

if ( is_admin()) {

	require_once( plugin_dir_path( __FILE__ ) . 'admin/class-pixpoints-admin.php' );
	add_action( 'plugins_loaded', array( 'PixPoints_Admin', 'get_instance' ) );

}
