<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 04/06/14
 * Time: 16:36
 */



$pixpoints_api = PIXPOINTS_API::get_instance();


$data = $pixpoints_api->get_participations('total', 'DESC', $instance['count']);

?>

<div class="widget leaderboard">
    <h4 itemprop="name" title="<?php echo $instance['title'] ?>"><?php echo $instance['title'] ?></h4>
    <ul>
    <?php foreach ($data as $key => $item): ?>

 <?php

        $fb_avatar = get_user_meta($item['uid'], 'facebook_avatar');

        $name = get_user_meta($item['uid'],'first_name',true);
        if(trim($name) == ''){
            $name = get_user_meta($item['uid'],'nickname',true);
        }

        $img = '';
        if ($fb_avatar)
            $img = "<img  width='50px' height='50px' src='{$fb_avatar[0]}' />";
        else
            $img = "" . get_avatar($item['user_login'], '50') . "";
        $img .= '<p class="user-name">' . $name . '</p>';

        ?>
        <li class="row">
            <?php echo $img; ?>
            <div class="col-md"><p><?php echo $item['total']; ?> points</p></div>
        </li>


    <?php endforeach; ?>
</ul>
    <div class="all-posts">
        <a class="green" href="/classement-general">Classement général</a>
    </div>
</div>
