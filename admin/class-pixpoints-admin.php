<?php
/**
 * Plugin Name.
 *
 * @package   PixPoints_Admin
 * @author    Pixweb médias <http://www.pixweb.ma/>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 Pixweb
 */

/**
 * Plugin class. This class should ideally be used to work with the
 * administrative side of the WordPress site.
 *
 * If you're interested in introducing public-facing
 * functionality, then refer to `class-pixpoints.php`
 *
 * @TODO: Rename this class to a proper name for your plugin.
 *
 * @package PixPoints_Admin
 * @author  Pixweb médias <http://www.pixweb.ma/>
 */
class PixPoints_Admin
{

    /**
     * Instance of this class.
     *
     * @since    1.0.0
     *
     * @var      object
     */
    protected static $instance = null;

    /**
     * Slug of the plugin screen.
     *
     * @since    1.0.0
     *
     * @var      string
     */
    protected $plugin_screen_hook_suffix = null;

    /**
     * Initialize the plugin by loading admin scripts & styles and adding a
     * settings page and menu.
     *
     * @since     1.0.0
     */
    private function __construct()
    {

        /*
         * @TODO :
         *
         * - Uncomment following lines if the admin class should only be available for super admins
         */
        if (!is_super_admin()) {
            return;
        }

        /*
         * Call $plugin_slug from public plugin class.
         *
         * @TODO:
         *
         * - Rename "PixPoints" to the name of your initial plugin class
         *
         */
        $plugin = PixPoints::get_instance();
        $this->plugin_slug = $plugin->get_plugin_slug();
        $pixpoints_api = PIXPOINTS_API::get_instance();


        // Load admin style sheet and JavaScript.
        add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_styles'));
        add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_scripts'));

        // Add the options page and menu item.
        add_action('admin_menu', array($this, 'add_plugin_admin_menu'));

        // Add an action link pointing to the options page.
        $plugin_basename = plugin_basename(plugin_dir_path(realpath(dirname(__FILE__))) . $this->plugin_slug . '.php');
        add_filter('plugin_action_links_' . $plugin_basename, array($this, 'add_action_links'));


    }


    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance()
    {

        /*
         * @TODO :
         *
         * - Uncomment following lines if the admin class should only be available for super admins
         */
        /* if( ! is_super_admin() ) {
            return;
        } */

        // If the single instance hasn't been set, set it now.
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Register and enqueue admin-specific style sheet.
     *
     * @TODO:
     *
     * - Rename "PixPoints" to the name your plugin
     *
     * @since     1.0.0
     *
     * @return    null    Return early if no settings page is registered.
     */
    public function enqueue_admin_styles()
    {

        if (!isset($this->plugin_screen_hook_suffix)) {
            return;
        }

        $screen = get_current_screen();
//        var_dump($this->plugin_screen_hook_suffix);
//        if ($this->plugin_screen_hook_suffix == $screen->id) {
        wp_enqueue_style($this->plugin_slug . '-admin-styles', plugins_url('assets/css/admin.css', __FILE__), array(), PixPoints::VERSION);
        wp_enqueue_style($this->plugin_slug . '-jquery-editable', plugins_url('assets/css/jqueryui-editable.css', __FILE__), array(), PixPoints::VERSION);

//        }

    }

    /**
     * Register and enqueue admin-specific JavaScript.
     *
     * @TODO:
     *
     * - Rename "PixPoints" to the name your plugin
     *
     * @since     1.0.0
     *
     * @return    null    Return early if no settings page is registered.
     */
    public function enqueue_admin_scripts()
    {

        if (!isset($this->plugin_screen_hook_suffix)) {
            return;
        }
//        var_dump($this->plugin_screen_hook_suffix == $screen->id);
//        if ($this->plugin_screen_hook_suffix == $screen->id) {
        wp_enqueue_script($this->plugin_slug . '-admin-script', plugins_url('assets/js/admin.js', __FILE__), array('jquery'), PixPoints::VERSION);
        wp_enqueue_script($this->plugin_slug . '-admin-datatable', plugins_url('assets/js/jquery.dataTables.min.js', __FILE__), array('jquery'), PixPoints::VERSION);
        wp_enqueue_script("myUi", "//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js");
        wp_enqueue_script($this->plugin_slug . '-admin-editable.js', plugins_url('assets/js/jqueryui-editable.js', __FILE__), array('jquery'), PixPoints::VERSION);

//        }

    }

    /**
     * Register the administration menu for this plugin into the WordPress Dashboard menu.
     *
     * @since    1.0.0
     */
    public function add_plugin_admin_menu()
    {

        /*
         * Add a settings page for this plugin to the Settings menu.
         *
         * NOTE:  Alternative menu locations are available via WordPress administration menu functions.
         *
         *        Administration Menus: http://codex.wordpress.org/Administration_Menus
         *
         * @TODO:
         *
         * - Change 'Page Title' to the title of your plugin admin page
         * - Change 'Menu Text' to the text for menu item for the plugin settings page
         * - Change 'manage_options' to the capability you see fit
         *   For reference: http://codex.wordpress.org/Roles_and_Capabilities
         */


        $this->plugin_screen_hook_suffix = add_menu_page(
            'PixPoints',
            'PixPoints',
            'manage_options',
            'pp_admin_manage',
            array($this, 'display_admin_page')
        );

        add_submenu_page('pp_admin_manage', 'PixPoints - ' . __('Manage', $this->plugin_slug), __('Manage', $this->plugin_slug), 'manage_options', 'pp_admin_manage', array($this, 'display_admin_page'));
        add_submenu_page('pp_admin_manage', 'PixPoints - ' . __('Configure', 'cp'), __('Configure', 'cp'), 'manage_options', 'display_configure_page', array($this, 'display_configure_page'));
        add_submenu_page('pp_admin_manage', 'PixPoints - ' . __('Features', 'cp'), __('Features', 'cp'), 'manage_options', 'display_features_page', array($this, 'display_features_page'));

    }

    /**
     * Render the settings page for this plugin.
     *
     */
    public function display_admin_page()
    {
        include_once(PIXPOINTS_PLUGIN_PATH . '/includes/class-pixpoints-list-table.php');
        include_once(PIXPOINTS_PLUGIN_PATH . '/includes/class-pixpoints-user-list-table.php');
        include_once('views/manage.php');
    }

    public function display_features_page()
    {
        if (isset($_POST['pp_admin_features_submit']) && $_POST['pp_admin_features_submit'] == 'Y') {

            if (isset($_POST['pp_enable_popup'])) {
                update_option('pp_enable_popup', 1);
            } else {
                update_option('pp_enable_popup', 0);
            }
            if (isset($_POST['pp_text_popup'])) {
                update_option('pp_text_popup', $_POST['pp_text_popup']);
            }
            if (isset($_POST['pp_max_day_points'])) {
                update_option('pp_max_day_points', $_POST['pp_max_day_points']);
            }


        }

        include_once('views/features.php');
    }

    public function display_configure_page()
    {


        if (isset($_POST['pp_admin_form_submit']) && $_POST['pp_admin_form_submit'] == 'Y') {


            $pp_enable_register = (isset($_POST['pp_enable_register']) ? $_POST['pp_enable_register'] : 0);
            $pp_enable_like_page = (isset($_POST['pp_enable_like_page']) ? $_POST['pp_enable_like_page'] : 0);
            $pp_enable_like_post = (isset($_POST['pp_enable_like_post']) ? $_POST['pp_enable_like_post'] : 0);
            $pp_enable_comment = (isset($_POST['pp_enable_comment']) ? $_POST['pp_enable_comment'] : 0);
            $pp_enable_follow = (isset($_POST['pp_enable_follow']) ? $_POST['pp_enable_follow'] : 0);
            $pp_enable_tweet = (isset($_POST['pp_enable_follow']) ? $_POST['pp_enable_follow'] : 0);
            $pp_enable_google = (isset($_POST['pp_enable_google']) ? $_POST['pp_enable_google'] : 0);
            $pp_enable_post_view = (isset($_POST['pp_enable_post_view']) ? $_POST['pp_enable_post_view'] : 0);
            $pp_enable_contest = (isset($_POST['pp_enable_contest']) ? $_POST['pp_enable_contest'] : 0);
            $pp_enable_share_contest = (isset($_POST['pp_enable_share_contest']) ? $_POST['pp_enable_share_contest'] : 0);

            update_option('pp_enable_register', $pp_enable_register);
            update_option('pp_enable_like_page', $pp_enable_like_page);
            update_option('pp_enable_like_post', $pp_enable_like_post);
            update_option('pp_enable_comment', $pp_enable_comment);
            update_option('pp_enable_follow', $pp_enable_follow);
            update_option('pp_enable_tweet', $pp_enable_tweet);
            update_option('pp_enable_google', $pp_enable_google);
            update_option('pp_enable_post_view', $pp_enable_post_view);
            update_option('pp_enable_contest', $pp_enable_contest);
            update_option('pp_enable_share_contest', $pp_enable_share_contest);


            // POINTS

            if (isset($_POST['pp_register_points']) && $pp_enable_register == 'on') {
                $pp_register_points = (int)$_POST['pp_register_points'];
                update_option('pp_register_points', $pp_register_points);
            }


            if (isset($_POST['pp_like_page_points']) && $pp_enable_like_page == 'on') {
                $pp_like_page_points = (int)$_POST['pp_like_page_points'];
                update_option('pp_like_page_points', $pp_like_page_points);
            }

            if (isset($_POST['pp_like_post_points']) && $pp_enable_like_post == 'on') {
                $pp_like_post_points = (int)$_POST['pp_like_post_points'];
                update_option('pp_like_post_points', $pp_like_post_points);
            }


            if (isset($_POST['pp_comment_points']) && $pp_enable_comment == 'on') {
                $pp_comment_points = (int)$_POST['pp_comment_points'];
                update_option('pp_comment_points', $pp_comment_points);
            }


            if (isset($_POST['pp_follow_points']) && $pp_enable_follow == 'on') {
                $pp_follow_points = (int)$_POST['pp_follow_points'];
                update_option('pp_follow_points', $pp_follow_points);
            }


            if (isset($_POST['pp_tweet_points']) && $pp_enable_tweet == 'on') {
                $pp_tweet_points = (int)$_POST['pp_tweet_points'];
                update_option('pp_tweet_points', $pp_tweet_points);
            }

            if (isset($_POST['pp_google_points']) && $pp_enable_tweet == 'on') {
                $pp_google_points = (int)$_POST['pp_google_points'];
                update_option('pp_google_points', $pp_google_points);
            }

            if (isset($_POST['pp_post_view_points']) && $pp_enable_post_view == 'on') {
                $pp_post_view_points = (int)$_POST['pp_post_view_points'];
                update_option('pp_post_view_points', $pp_post_view_points);
            }

            if (isset($_POST['pp_attend_contest_points']) && $pp_enable_post_view == 'on') {
                $pp_attend_contest_points = (int)$_POST['pp_attend_contest_points'];
                update_option('pp_attend_contest_points', $pp_attend_contest_points);
            }

            if (isset($_POST['pp_share_contest_points']) && $pp_enable_share_contest == 'on') {
                $pp_share_contest_points = (int)$_POST['pp_share_contest_points'];
                update_option('pp_share_contest_points', $pp_share_contest_points);
            }


            // POPUPS TEXT


            if (isset($_POST['pp_text_like_page']) && $pp_enable_like_page == 'on') {
                update_option('pp_text_like_page', $_POST['pp_text_like_page']);
            }


            if (isset($_POST['pp_text_like_post']) && $pp_enable_register == 'on') {
                update_option('pp_text_like_post', $_POST['pp_text_like_post']);
            }

            if (isset($_POST['pp_text_follow']) && $pp_enable_follow == 'on') {
                update_option('pp_text_follow', $_POST['pp_text_follow']);
            }


            if (isset($_POST['pp_text_tweet']) && $pp_enable_tweet == 'on') {
                update_option('pp_text_tweet', $_POST['pp_text_tweet']);
            }

            if (isset($_POST['pp_text_google']) && $pp_enable_tweet == 'on') {
                update_option('pp_text_google', $_POST['pp_text_google']);
            }



        }

        include_once('views/configure.php');

    }

    /**
     * Add settings action link to the plugins page.
     *
     * @since    1.0.0
     */
    public function add_action_links($links)
    {

        return array_merge(
            array(
                'settings' => '<a href="' . admin_url('options-general.php?page=' . $this->plugin_slug) . '">' . __('Settings', $this->plugin_slug) . '</a>'
            ),
            $links
        );

    }

    /**
     * NOTE:     Actions are points in the execution of a page or process
     *           lifecycle that WordPress fires.
     *
     *           Actions:    http://codex.wordpress.org/Plugin_API#Actions
     *           Reference:  http://codex.wordpress.org/Plugin_API/Action_Reference
     *
     * @since    1.0.0
     */
    public function action_method_name()
    {
        // @TODO: Define your action hook callback here
    }

    /**
     * NOTE:     Filters are points of execution in which WordPress modifies data
     *           before saving it or sending it to the browser.
     *
     *           Filters: http://codex.wordpress.org/Plugin_API#Filters
     *           Reference:  http://codex.wordpress.org/Plugin_API/Filter_Reference
     *
     * @since    1.0.0
     */
    public function filter_method_name()
    {
        // @TODO: Define your filter hook callback here
    }

}
