(function ($) {
    "use strict";

    $(function () {

        var $checkbox = $("form#pp_admin_form input:checkbox")
        $checkbox.click(function () {
            var $parent = $(this).parent('tr');
            console.log($parent.find('textarea').text());
            if (this.checked) {
                $(this).prev().attr('disabled', false);
                $('.'+$(this).attr('id')).attr('disabled', false);
            } else {
                $(this).prev().attr('disabled', true)
                $('.'+$(this).attr('id')).attr('disabled', true);
            }
        });

    });

}(jQuery));