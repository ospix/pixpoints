<?php
/**
 * Represents the view for the administration dashboard.
 *
 * This includes the header, options, and other information that should provide
 * The User Interface to the end user.
 *
 * @package   PixPoints
 * @author    Pixweb médias <http://www.pixweb.ma/>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 Pixweb
 */
?>

<div class="wrap">

    <h2><?php echo esc_html(get_admin_page_title()); ?></h2>


    <form name="pp_admin_form" method="post">
        <input type="hidden" name="pp_admin_features_submit" value="Y"/>

        <h3><?php _e('Social Settings', 'pp'); ?></h3>

        <table class="form-table">
            <tr valign="top">
                <th scope="row"><label for="pp_register_points"><?php _e('Show Popups', 'pp'); ?>
                        :</label></th>
                <td valign="middle" colspan="2">
                    <input type="checkbox" name="pp_enable_popup"
                           id="pp_enable_popup" <?php if (get_option('pp_enable_popup') == '1'): ?> checked <?php endif; ?> />
                </td>
            </tr>


            <tr valign="top">
                <th scope="row"><label for="pp_register_points"><?php _e('Popups Text', 'pp'); ?>
                        :</label></th>
                <td valign="middle" colspan="2">

                    <textarea name="pp_text_popup" id="pp_text_popup" cols="40" rows="5"><?php echo stripslashes(get_option('pp_text_popup',''));?></textarea>
                </td>
            </tr>

            <tr valign="top">
                <th scope="row"><label for="pp_max_day_points"><?php _e('Max Points Day', 'pp'); ?>
                        :</label></th>
                <td valign="middle" colspan="2">
                    <input type="text" id="pp_max_day_points" name="pp_max_day_points"
                           value="<?php echo get_option('pp_max_day_points'); ?>"
                           size="30"/>
            </tr>


        </table>
        <br/>

        <p class="submit">
            <input type="submit" name="Submit" value="<?php _e('Update Options', 'pp'); ?>"/>
        </p>

    </form>


</div>
