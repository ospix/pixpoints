<?php
/**
 * Represents the view for the administration dashboard.
 *
 * This includes the header, options, and other information that should provide
 * The User Interface to the end user.
 *
 * @package   PixPoints
 * @author    Pixweb médias <http://www.pixweb.ma/>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 Pixweb
 */

/** Formats points with prefix and suffix */
function pp_formatPoints($points)
{
    if ($points == 0) {
        $points = '0';
    }
    return get_option('pp_prefix') . $points . get_option('pp_suffix');
}


function pp_displayPoints($uid = 0, $return = 0, $format = 1)
{
    if ($uid == 0) {
        if (!is_user_logged_in()) {
            return false;
        }
        $uid = pp_currentUser();
    }

    if ($format == 1) {
        $fpoints = pp_formatPoints(pp_getPoints($uid));
    } else {
        $fpoints = pp_getPoints($uid);
    }

    if (!$return) {
        echo $fpoints;
    } else {
        return $fpoints;
    }
}

function pp_getPoints($uid)
{
    $points = get_user_meta($uid, 'ppoints', 1);
    if ($points == '') {
        return 0;
    } else {
        return $points;
    }
}


function pp_currentUser()
{
    require_once(ABSPATH . WPINC . '/pluggable.php');
    global $current_user;
    get_currentuserinfo();
    return $current_user->ID;
}


function getPointsDb($uid)
{
    global $wpdb;

    return (int)$wpdb->get_var('SELECT SUM(points) FROM `' . $wpdb->base_prefix . 'pixpoints' . '` where  `uid`=' . $uid);
}


?>

    <div class="wrap">
        <h2>PixPoints - <?php _e('Manage', 'pp'); ?></h2>
        <?php _e('Manage the points of your users.', 'pp'); ?><br/><br/>

        <div class="updated" id="pp_manage_updated" style="display: none;"></div>

        <form action="<?php echo admin_url('admin.php?page=pp_admin_manage');?>" method="post">


        <?php
        global $wpdb;

        if ($_GET['page'] == 'pp_admin_manage' && isset($_GET['id'])) {

            $user = get_user_by('id',$_GET['id']);
            $img ='';
            $fb_avatar = get_user_meta($user->ID, 'facebook_avatar');
            if ($fb_avatar)
                $img .= "<div style='float: left; margin-right: 10px'><img  width='40px' height='40px' src='{$fb_avatar[0]}' /></div>";
            else
                $img = "<div style='float: left; margin-right: 10px'>" . get_avatar($user->data->user_login, '40') . "</div>";
            $img .= '<strong>' . $user->data->user_nicename . '</strong><br/><i>' . $user->data->user_email . '</i>';

            echo  $img;


            $pixpoints_user_list_table = new PIXPOINTS_User_List_Table();
            $pixpoints_user_list_table->prepare_items($_GET['id']);

            $pixpoints_user_list_table->display();

        } else {

            $pixpoints_list_table = new PIXPOINTS_List_Table();
            $pixpoints_list_table->prepare_items();

            $pixpoints_list_table->display();
        }

        ?>
        </form>

    </div>



<?php do_action('pp_admin_manage'); ?>