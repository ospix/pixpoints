<?php
/**
 * Represents the view for the administration dashboard.
 *
 * This includes the header, options, and other information that should provide
 * The User Interface to the end user.
 *
 * @package   PixPoints
 * @author    Pixweb médias <http://www.pixweb.ma/>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 Pixweb
 */
?>

<?php


$pp_enable_register = get_option('pp_enable_register', 'on');
$pp_enable_like_page = get_option('pp_enable_like_page', 'on');
$pp_enable_like_post = get_option('pp_enable_like_post', 'on');
$pp_enable_comment = get_option('pp_enable_comment', 'on');
$pp_enable_follow = get_option('pp_enable_follow', 'on');
$pp_enable_tweet = get_option('pp_enable_tweet', 'on');
$pp_enable_google = get_option('pp_enable_google', 'on');
$pp_enable_post_view = get_option('pp_enable_post_view', 'on');
$pp_enable_contest = get_option('pp_enable_contest', 'on');
$pp_enable_share_contest = get_option('pp_enable_share_contest', 'on');

?>
<div class="wrap">

    <h2><?php echo esc_html(get_admin_page_title()); ?></h2>


    <form id="pp_admin_form" name="pp_admin_form" method="post">
        <input type="hidden" name="pp_admin_form_submit" value="Y"/>

        <h3><?php _e('Social Settings', 'pp'); ?></h3>
        <table class="form-table">
            <tr valign="top">
                <th scope="row"><label for="pp_register_points"><?php _e('Inscription', 'pp'); ?>
                        :</label></th>
                <td valign="middle" colspan="2">
                    <input type="text" id="pp_register_points" name="pp_register_points"
                           value="<?php echo get_option('pp_register_points'); ?>"
                           size="30" <?php if ($pp_enable_register !== "on"): ?> disabled="disabled" <?php endif; ?>/>
                    <input type="checkbox" name="pp_enable_register"
                           id="pp_enable_register" <?php if ($pp_enable_register === 'on'): ?> checked <?php endif; ?> />

                </td>
            </tr>


            <tr valign="top">
                <th scope="row"><label for="pp_like_page_points"><?php _e('Facebook Fanpage like', 'pp'); ?>
                        :</label></th>
                <td valign="middle" colspan="2">
                    <input type="text" id="pp_like_page_points" name="pp_like_page_points"
                           value="<?php echo get_option('pp_like_page_points'); ?>"
                           size="30" <?php if ($pp_enable_like_page !== "on"): ?> disabled="disabled" <?php endif; ?>/>
                    <input type="checkbox" name="pp_enable_like_page"
                           id="pp_enable_like_page" <?php if ($pp_enable_like_page === 'on'): ?> checked <?php endif; ?> />

                </td>

            <tr valign="top">
                <th scope="row"></th>
                <td valign="middle">
                    <textarea class="pp_enable_like_page" name="pp_text_like_page" id="pp_text_like_page"
                              cols="30"  <?php if ($pp_enable_like_page !== "on"): ?> disabled="disabled" <?php endif; ?>
                              rows="5"><?php echo stripslashes(get_option('pp_text_like_page', '')); ?></textarea>
                </td>
            </tr>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="pp_like_post_points"><?php _e('Facebook single post like', 'pp'); ?>
                        :</label></th>
                <td valign="middle" colspan="2">
                    <input type="text" id="pp_like_post_points"
                           name="pp_like_post_points" <?php if ($pp_enable_like_post !== "on"): ?> disabled="disabled" <?php endif; ?>
                           value="<?php echo get_option('pp_like_post_points'); ?>" size="30"/>
                    <input type="checkbox" name="pp_enable_like_post"
                           id="pp_enable_like_post" <?php if ($pp_enable_like_post === 'on'): ?> checked <?php endif; ?> />
                </td>
            <tr valign="top">
                <th scope="row"></th>
                <td valign="middle" colspan="2">
                    <textarea class="pp_enable_like_post" name="pp_text_like_post" id="pp_text_like_post"
                              cols="30" <?php if ($pp_enable_like_post !== "on"): ?> disabled="disabled" <?php endif; ?>
                              rows="5"><?php echo stripslashes(get_option('pp_text_like_post', '')); ?></textarea>
                </td>
            </tr>


            </tr>
            <tr valign="top">
                <th scope="row"><label for="pp_comment_points"><?php _e('Facebook comment', 'pp'); ?>:</label>
                </th>
                <td valign="middle" colspan="2">
                    <input type="text" id="pp_comment_points"
                           name="pp_comment_points" <?php if ($pp_enable_comment !== "on"): ?> disabled="disabled" <?php endif; ?>
                           value="<?php echo get_option('pp_comment_points'); ?>" size="30"/>
                    <input type="checkbox" name="pp_enable_comment"
                           id="pp_enable_comment" <?php if ($pp_enable_comment === 'on'): ?> checked <?php endif; ?> />
                </td>

            </tr>

            <tr valign="top">
                <th scope="row"><label for="pp_follow_points"><?php _e('Twitter follow', 'pp'); ?>:</label>
                </th>
                <td valign="middle" colspan="2">
                    <input type="text" id="pp_follow_points"
                           name="pp_follow_points" <?php if ($pp_enable_follow !== "on"): ?> disabled="disabled" <?php endif; ?>
                           value="<?php echo get_option('pp_follow_points'); ?>" size="30"/>
                    <input type="checkbox" name="pp_enable_follow"
                           id="pp_enable_follow" <?php if ($pp_enable_follow === 'on'): ?> checked <?php endif; ?> />

                </td>
            <tr valign="top">
                <th scope="row"></th>
                <td valign="middle">
                    <textarea class="pp_enable_follow" name="pp_text_follow" id="pp_text_follow"
                              cols="30" <?php if ($pp_enable_follow !== "on"): ?> disabled="disabled" <?php endif; ?>
                              rows="5"><?php echo stripslashes(get_option('pp_text_follow', '')); ?></textarea>
                </td>
            </tr>

            </tr>
            <tr valign="top">
                <th scope="row"><label for="pp_tweet_points"><?php _e('Twitter tweet', 'pp'); ?>:</label>
                </th>
                <td valign="middle" colspan="2">
                    <input type="text" id="pp_tweet_points"
                           name="pp_tweet_points" <?php if ($pp_enable_tweet !== "on"): ?> disabled="disabled" <?php endif; ?>
                           value="<?php echo get_option('pp_tweet_points'); ?>" size="30"/>
                    <input type="checkbox" name="pp_enable_tweet"
                           id="pp_enable_tweet" <?php if ($pp_enable_tweet === 'on'): ?> checked <?php endif; ?> />

                </td>
            <tr valign="top">
                <th scope="row"></th>
                <td valign="middle">
                    <textarea class="pp_enable_tweet" name="pp_text_tweet" id="pp_text_tweet"
                              cols="30"  <?php if ($pp_enable_tweet !== "on"): ?> disabled="disabled" <?php endif; ?>
                              rows="5"><?php echo stripslashes(get_option('pp_text_tweet', '')); ?></textarea>
                </td>
            </tr>

            </tr>
            <tr valign="top">
                <th scope="row"><label for="pp_google_points"><?php _e('Google +', 'pp'); ?>:</label>
                </th>
                <td valign="middle" colspan="2">
                    <input type="text" id="pp_google_points"
                           name="pp_google_points" <?php if ($pp_enable_google !== "on"): ?> disabled="disabled" <?php endif; ?>
                           value="<?php echo get_option('pp_google_points'); ?>" size="30"/>
                    <input type="checkbox" name="pp_enable_google"
                           id="pp_enable_google" <?php if ($pp_enable_google === 'on'): ?> checked <?php endif; ?> />

                </td>

            <tr valign="top">
                <th scope="row"></th>
                <td valign="middle">
                    <textarea class="pp_enable_google" name="pp_text_google" id="pp_text_google"
                              cols="30" <?php if ($pp_enable_google !== "on"): ?> disabled="disabled" <?php endif; ?>
                              rows="5"><?php echo stripslashes(get_option('pp_text_google', '')); ?></textarea>
                </td>
            </tr>

            </tr>
            <tr valign="top">
                <th scope="row"><label for="pp_post_view_points"><?php _e('Post View', 'pp'); ?>:</label>
                </th>
                <td valign="middle" colspan="2">
                    <input type="text" id="pp_post_view_points"
                           name="pp_post_view_points" <?php if ($pp_enable_post_view !== "on"): ?> disabled="disabled" <?php endif; ?>
                           value="<?php echo get_option('pp_post_view_points'); ?>" size="30"/>
                    <input type="checkbox" name="pp_enable_post_view"
                           id="pp_enable_post_view" <?php if ($pp_enable_post_view === 'on'): ?> checked <?php endif; ?> />

                </td>

            </tr>

            <tr valign="top">
                <th scope="row"><label for="pp_attend_contest"><?php _e('Participation Contest', 'pp'); ?>:</label>
                </th>
                <td valign="middle" colspan="2">
                    <input type="text" id="pp_attend_contest"
                           name="pp_attend_contest_points" <?php if ($pp_enable_contest !== "on"): ?> disabled="disabled" <?php endif; ?>
                           value="<?php echo get_option('pp_attend_contest_points'); ?>" size="30"/>
                    <input type="checkbox" name="pp_enable_contest"
                           id="pp_enable_contest" <?php if ($pp_enable_contest === 'on'): ?> checked <?php endif; ?> />

                </td>

            </tr>

            <tr valign="top">
                <th scope="row"><label for="pp_share_contest_points"><?php _e('Partage Contest', 'pp'); ?>:</label>
                </th>
                <td valign="middle" colspan="2">
                    <input type="text" id="pp_share_contest_points"
                           name="pp_share_contest_points" <?php if ($pp_enable_share_contest !== "on"): ?> disabled="disabled" <?php endif; ?>
                           value="<?php echo get_option('pp_share_contest_points'); ?>" size="30"/>
                    <input type="checkbox" name="pp_enable_share_contest"
                           id="pp_enable_share_contest" <?php if ($pp_enable_share_contest === 'on'): ?> checked <?php endif; ?> />

                </td>

            </tr>


        </table>
        <br/>

        <p class="submit">
            <input type="submit" name="Submit" value="<?php _e('Update Options', 'pp'); ?>"/>
        </p>

    </form>


</div>
